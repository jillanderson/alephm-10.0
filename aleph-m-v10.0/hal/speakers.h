/*******************************************************************************
 *
 * obsługa przyłączania zespołów głośnikowych do końcówki mocy
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>

/*
 * inicjalizacja - głośniki odłączone
 */
void hal_speakers_init(void);

/*
 * przyłącza głośniki
 */
void hal_speakers_connect(void);

/*
 * odłącza głośniki
 */
void hal_speakers_disconnect(void);

/*
 * przełącza głośniki w stan przeciwny
 */
void hal_speakers_toggle(void);

/*
 * sprawdza czy głośniki są podłączone
 */
bool hal_speakers_are_connected(void);

