#include "../indicator.h"

#include <avr/io.h>
#include <stdbool.h>

#include "../../common/io/io.h"

static io_st const io_indicator = { .port = &PORTB, .pin = PB2 };

void hal_indicator_init(void)
{
	io_set_low(&io_indicator);
	io_set_as_output(&io_indicator);
}

void hal_indicator_on(void)
{
	io_set_high(&io_indicator);
}

void hal_indicator_off(void)
{
	io_set_low(&io_indicator);
}

void hal_indicator_toggle(void)
{
	io_set_opposite(&io_indicator);
}

bool hal_indicator_is_on(void)
{
	return ((0U == io_read_pin(&io_indicator)) ? false : true);
}

