#include "../key.h"

#include <avr/io.h>
#include <stdbool.h>

#include "../../common/io/io.h"

static io_st const io_key = { .port = &PORTB, .pin = PB1 };

void hal_key_init(void)
{
	io_set_low(&io_key); /* wyłączamy wewnętrzny pullup */
	io_set_as_input(&io_key);
}

bool hal_key_is_pressed(void)
{
	return ((0U == io_read_pin(&io_key)) ? true : false);
}

