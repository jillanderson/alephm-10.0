#include "../mcu_gpio.h"

#include <avr/io.h>
#include <stdint.h>

#include "../../common/port/port.h"

void hal_mcu_gpio_init(void)
{
	static uint8_t const pullup_on = 0xFFU;

	port_set_value(&PORTB, pullup_on);
	port_set_value(&PORTC, pullup_on);
	port_set_value(&PORTD, pullup_on);
}

