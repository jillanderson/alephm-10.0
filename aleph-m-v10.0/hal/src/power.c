#include "../power.h"

#include <avr/io.h>
#include <stdbool.h>

#include "../../common/io/io.h"

static io_st const io_power = { .port = &PORTC, .pin = PC3 };

void hal_power_init(void)
{
	io_set_low(&io_power);
	io_set_as_output(&io_power);
}

void hal_power_on(void)
{
	io_set_high(&io_power);
}

void hal_power_off(void)
{
	io_set_low(&io_power);
}

void hal_power_toggle(void)
{
	io_set_opposite(&io_power);
}

bool hal_power_is_on(void)
{
	return ((0U == io_read_pin(&io_power)) ? false : true);
}

