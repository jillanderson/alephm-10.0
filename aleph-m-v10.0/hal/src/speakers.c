#include "../speakers.h"

#include <avr/io.h>
#include <stdbool.h>

#include "../../common/io/io.h"

static io_st const io_speaker_left = { .port = &PORTC, .pin = PC4 };
static io_st const io_speaker_right = { .port = &PORTC, .pin = PC5 };

void hal_speakers_init(void)
{
	io_set_low(&io_speaker_left);
	io_set_as_output(&io_speaker_left);
	io_set_low(&io_speaker_right);
	io_set_as_output(&io_speaker_right);
}

void hal_speakers_connect(void)
{
	io_set_high(&io_speaker_left);
	io_set_high(&io_speaker_right);
}

void hal_speakers_disconnect(void)
{
	io_set_low(&io_speaker_left);
	io_set_low(&io_speaker_right);
}

void hal_speakers_toggle(void)
{
	io_set_opposite(&io_speaker_left);
	io_set_opposite(&io_speaker_right);
}

bool hal_speakers_are_connected(void)
{
	return (((0U == io_read_pin(&io_speaker_left))
	                || (0U == io_read_pin(&io_speaker_right))) ?
	                false : true);
}

