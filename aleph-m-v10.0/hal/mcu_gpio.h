/*******************************************************************************
 *
 * początkowa konfiguracja portów gpio mikrokontrolera
 *
 ******************************************************************************/

#pragma once

/*
 * ustawia stan początkowy portów gpio
 */
void hal_mcu_gpio_init(void);

