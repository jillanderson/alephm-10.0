/*******************************************************************************
 *
 * obsługa sterowania zasilaniem
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>

/*
 * inicjalizacja - zasilanie wyłączone
 */
void hal_power_init(void);

/*
 * włącza zasilanie
 */
void hal_power_on(void);

/*
 * wyłącza zasilanie
 */
void hal_power_off(void);

/*
 * przełącza zasilanie w stan przeciwny
 */
void hal_power_toggle(void);

/*
 * sprawdza czy zasilanie jest włączone
 */
bool hal_power_is_on(void);

