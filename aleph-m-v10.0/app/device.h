/*******************************************************************************
 *
 * sterowanie urządzeniem
 *
 ******************************************************************************/

#pragma once

/*
 * inicjuje aplikację
 */
void device_init(void);

/*
 * steruje urządzeniem
 * musi być wywoływana w nieskończonej pętli funkcji main
 */
void device_process(void);

