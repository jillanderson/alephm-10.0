/*******************************************************************************
 *
 * obsługa podstawy czasu 1ms
 *
 ******************************************************************************/

#pragma once

#include "../hal/mcu_timer2.h"

/*
 * uruchamia podstawę czasu
 */
void timestamp_start(timer2_handler_ft *handler, void *arg);

/*
 * zatrzymuje podstawę czasu
 */
void timestamp_stop(void);

