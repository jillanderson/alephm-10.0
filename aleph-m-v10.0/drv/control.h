/*******************************************************************************
 *
 * obsługa przycisku sterującego
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>

#include "../common/key/key.h"

/*
 * inicjalizacja
 *
 * @debounce_time		czas debouncingu
 * @double_press_gap_time	czas wykrycia podwójnego wciśnięcia
 * @long_press_start_time	czas wykrycia długiego wciśnięcia
 * @long_press_repeat_time	czas repetycji przy długim wciśnięciu
 */
void control_init(uint8_t debounce_time, uint16_t double_press_gap_time,
                 uint16_t long_press_start_time,
                 uint16_t long_press_repeat_time);

/*
 * rejestruje funkcję obsługi zdarzeń
 */
void control_handler(key_handler_ft *handler, void *arg);

/*
 * przetwarza zarejestrowane zadanie
 * musi być wywoływana w pętli głównej programu
 */
void control_dispatch(void);

/*
 * uaktualnia stan systemu wykrywania zdarzeń od przycisku
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void control_on_tick(void);

