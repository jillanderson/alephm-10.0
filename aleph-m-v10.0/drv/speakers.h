/*******************************************************************************
 *
 * sterowanie podłączaniem zespołów głośnikowych do wyjść końcówki mocy
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>

/*
 * inicjalizacja - głośniki odłączone
 */
void speakers_init(void);

/*
 * przyłącza głośniki z zadaną zwłoką czasową
 */
void speakers_connect(uint16_t delay);

/*
 * odłącza głośniki bezzwłocznie
 */
void speakers_disconnect(void);

/*
 * sprawdza czy głośniki są podłączone
 */
bool speakers_are_connected(void);

/*
 * przetwarza zarejestrowane zadanie
 * musi być wywoływana w pętli głównej programu
 */
void speakers_dispatch(void);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void speakers_on_tick(void);

