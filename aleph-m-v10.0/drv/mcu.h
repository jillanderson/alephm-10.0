/*******************************************************************************
 *
 * konfiguracja mikrokontrolera
 *
 ******************************************************************************/

#pragma once

/*
 * inicjalizacja - ustawienia procesora
 * powinna być wywoływana jako pierwsza w aplikacji
 */
void mcu_init(void);

