/*******************************************************************************
 *
 * sterowanie zasilaniem
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>

/*
 * inicjalizacja - zasilanie wyłączone
 */
void power_init(void);

/*
 * włącza zasilanie
 */
void power_on(void);

/*
 * wyłącza zasilanie
 */
void power_off(void);

/*
 * przełącza zasilanie w stan przeciwny
 */
void power_toggle(void);

/*
 * sprawdza czy zasilanie jest włączone
 */
bool power_is_on(void);

