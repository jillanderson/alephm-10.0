#include "../../hal/power.h"
#include "../power.h"

#include <stdbool.h>

void power_init(void)
{
	hal_power_init();
}

void power_on(void)
{
	hal_power_on();
}

void power_off(void)
{
	hal_power_off();
}

void power_toggle(void)
{
	hal_power_toggle();
}

bool power_is_on(void)
{
	return (hal_power_is_on());
}

