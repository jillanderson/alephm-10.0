#include "../../hal/speakers.h"
#include "../speakers.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "../../common/timer/timer.h"

static struct {
	timer_st delay;
} speakers = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

static void delay_handler(void *arg);

void speakers_init(void)
{
	hal_speakers_init();
	timer_init(&(speakers.delay));
	timer_handler(&(speakers.delay), delay_handler, NULL);
}

void speakers_connect(uint16_t delay)
{
	timer_start(&(speakers.delay), delay);
}

void speakers_disconnect(void)
{
	hal_speakers_disconnect();
	timer_stop(&(speakers.delay));
}

bool speakers_are_connected(void)
{
	return (hal_speakers_are_connected());
}

void speakers_dispatch(void)
{
	timer_dispatch(&(speakers.delay));
}

void speakers_on_tick(void)
{
	timer_on_tick(&(speakers.delay));
}

void delay_handler(void *arg __attribute__((unused)))
{
	hal_speakers_connect();
}

