#include "../timestamp.h"

#include <stdint.h>

#include "../../hal/mcu_timer2.h"

void timestamp_start(timer2_handler_ft *handler, void *arg)
{
	static uint8_t const t2_top = 249U;

	hal_mcu_timer2_start(TIMER2_PRESCALER_32, t2_top);
	hal_mcu_timer2_handler(handler, arg);
}

void timestamp_stop(void)
{
	hal_mcu_timer2_stop();
}

