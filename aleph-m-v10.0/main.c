/*******************************************************************************
 *
 * Sterownik wzmacniacza Aleph-M
 *
 *******************************************************************************
 * Copyright (C) 2023r. GNU Public License
 * Version 10.0
 *******************************************************************************
 * funkcje:
 * 	- sterowanie włączaniem i wyłączaniem
 * 	- opóźnione załączanie głośników przy włączeniu
 * 	- bezzwłoczne odłączanie głośników przy wyłączeniu
 *
 * sterowanie:
 * 	- krótkie naciśnięcie przycisku: zasilanie włącz/wyłącz
 *
 * zasoby sprzętowe:
 * 	- CPU			ATmega8
 * 	- F_CPU			8MHz oscylator wewnętrzny
 * 	- BOD			max 4,5V
 * 	- TIMER2		systemowa podstawa czasu
 *******************************************************************************
 * kompilator: avr-gcc 12.1.0
 *******************************************************************************
 * opcje avr
 * ---------
 * 	-mrelax			dodaje opcje --mlink-relax do linii
 * 				komend assemblera oraz --relax do linii komend
 * 				linkera)
 * 	-mcall-prologues	rozwija prologi/epilogi funkcji w odrębne
 * 				podprogramy
 * 	-maccumulate-args	może prowadzić do zmniejszenia rozmiaru kodu
 * 				dla funkcji, które wykonują kilka wywołań
 *				i które otrzymują swoje argumenty na stosie
 * 	-mstrict-X		użycie rejestru X w sposób proponowany przez
 * 				sprzęt. Używany w adresowaniu pośrednim.
 * 				(tu: nieużywana)
 *
 * opcje gcc
 * ---------
 * 	-std=gnu99		standard c99 i rozszerzenia gnu
 * 	-03			poziom optymalizacji
 * 	-fshort-enums		najkrótszy typ mieszczący wszystkie wartości
 *	-ffunction-sections	odrębne sekcje dla funkcji
 *	-fdata-sections		odrębne sekcje dla danych
 *	-funsigned-char		typ char bez znaku
 *	-funsigned-bitfields	pola bitowe bez znaku
 *	-flto			link time optimization
 *	-fno-common		niezainicjowane zmienne globalne w sekcji BSS
 *	-fanalyzer		włącza statyczny analizator kodu
 *
 * podstawowe ostrzeżenia
 * ----------------------
 *	-Wall
 *	-Wextra
 *	-Werror
 *
 * dodatkowe ostrzeżenia nie włączane przez -Wall -Wextra
 * ------------------------------------------------------
 * 	-Walloc-zero
 * 	-Warith-conversion
 * 	-Wbad-function-cast
 * 	-Wcast-qual
 * 	-Wconversion
 * 	-Wdouble-promotion
 * 	-Wduplicated-branches
 * 	-Wduplicated-cond
 * 	-Wfloat-conversion
 * 	-Wfloat-equal
 * 	-Winit-self
 * 	-Wjump-misses-init
 * 	-Wlogical-op
 * 	-Wmissing-declarations
 * 	-Wmissing-include-dirs
 * 	-Wmissing-prototypes
 * 	-Wmultichar
 * 	-Wnested-externs
 * 	-Wopenacc-parallelism
 * 	-Woverlength-strings
 * 	-Wpointer-arith
 * 	-Wredundant-decls
 * 	-Wsign-conversion
 * 	-Wstrict-prototypes
 * 	-Wsuggest-attribute=cold
 * 	-Wsuggest-attribute=const
 * 	-Wsuggest-attribute=malloc
 * 	-Wsuggest-attribute=noreturn
 * 	-Wsuggest-attribute=pure
 * 	-Wswitch-default
 * 	-Wundef
 * 	-Wunsuffixed-float-constants
 * 	-Wunused-macros
 * 	-Wvariadic-macros
 * 	-Wwrite-strings
 *	-Wcast-align
 *	-Winline
 *	-Wmissing-format-attribute
 *	-Wnull-dereference
 *	-Wpacked
 *	-Wpadded
 *	-Wrestrict
 *	-Wshadow
 *	-Wunsafe-loop-optimizations
 *
 * opcje linkera
 * -------------
 *	-Wl,--gc-sections,--warn-common
 *
 ******************************************************************************/

#include <stdlib.h>

#include "app/device.h"

__attribute__((OS_main)) int main(void)
{
	device_init();

	for (;;) {
		device_process();
	}

	return (EXIT_SUCCESS);
}

