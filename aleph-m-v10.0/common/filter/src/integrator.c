#include "../integrator.h"

#include <stdint.h>

void integrator_init(integrator_st *const integrator, uint8_t steps)
{
	integrator->steps = steps;
	integrator->counter = 0U;
	integrator->level = INTEGRATOR_LEVEL_LOW;
}

integrator_level_et integrator_level(integrator_st *const integrator)
{
	if (0U == integrator->counter) {
		integrator->level = INTEGRATOR_LEVEL_LOW;
	} else if ((integrator->steps) == (integrator->counter)) {
		integrator->level = INTEGRATOR_LEVEL_HIGH;
	}

	return (integrator->level);
}

void integrator_on_tick(integrator_st *const integrator, integrator_level_et level)
{
	if (INTEGRATOR_LEVEL_LOW == level) {
		if (0U < integrator->counter) {
			integrator->counter--;
		}
	} else {
		if (integrator->steps > integrator->counter) {
			integrator->counter++;
		}
	}
}

