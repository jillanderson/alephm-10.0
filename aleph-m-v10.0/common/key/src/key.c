#include "../key.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

enum key_state {
	KEY_STATE_00 = 0U,
	KEY_STATE_10 = 10U,
	KEY_STATE_20 = 20U,
	KEY_STATE_30 = 30U,
	KEY_STATE_40 = 40U
};

void key_init(key_st *const key, uint16_t double_press_gap_time,
              uint16_t long_press_start_time, uint16_t long_press_repeat_time)
{
	key->double_press_gap_time = double_press_gap_time;
	key->long_press_start_time = long_press_start_time;
	key->long_press_repeat_time = long_press_repeat_time;
	key->counter = 0U;
	key->state = KEY_STATE_00;
	key->event = KEY_NOT_PRESSED_EVENT;
	key->handler = NULL;
	key->arg = NULL;
}

void key_handler(key_st *const key, key_handler_ft *handler, void *arg)
{
	key->handler = handler;
	key->arg = arg;
}

void key_dispatch(key_st *const key)
{
	if (KEY_NOT_PRESSED_EVENT != key->event) {
		if (NULL != key->handler) {
			(key->handler)(key->event, key->arg);
		}

		key->event = KEY_NOT_PRESSED_EVENT;
	}
}

void key_on_tick(key_st *const key, bool pressed)
{
	if (0U < (key->counter)) {
		(key->counter)--;
	}

	switch (key->state) {
	case KEY_STATE_00:
		/* czekamy na wciśnięcie przycisku */
		if (true == pressed) {
			key->counter = key->long_press_start_time;
			key->state = KEY_STATE_10;
		}

		break;

	case KEY_STATE_10:
		if (true == pressed) {
			/* wykrycie długiego wciśnięcia */
			if (0U == (key->counter)) {
				key->counter = key->long_press_repeat_time;
				key->event = KEY_LONG_PRESS_START_EVENT;
				key->state = KEY_STATE_40;
			}
		} else {
			/* wcześniejsze zwolnienie przycisku */
			key->counter = key->double_press_gap_time;
			key->state = KEY_STATE_20;
		}

		break;

	case KEY_STATE_20:
		if (0U == (key->counter)) {
			/* pojedyńcze wciśnięcie */
			key->event = KEY_SHORT_PRESSED_EVENT;
			key->state = KEY_STATE_00;
		} else {
			/* dwuklik */
			if (true == pressed) {
				key->counter = 0U;
				key->state = KEY_STATE_30;
			}
		}

		break;

	case KEY_STATE_30:
		/* oczekiwanie na zwolnienie przycisku po dwukliku */
		if (false == pressed) {
			key->event = KEY_DOUBLE_PRESSED_EVENT;
			key->state = KEY_STATE_00;
		}

		break;

	case KEY_STATE_40:
		if (true == pressed) {
			/* długie wciśnięcie, wykonaj akcję cykliczną */
			if (0U == key->counter) {
				key->counter = key->long_press_repeat_time;
				key->event = KEY_LONG_PRESSED_EVENT;
			}
		} else {
			/* zwolnienie przycisku po długim wciśnięciu */
			key->counter = 0U;
			key->event = KEY_LONG_PRESS_END_EVENT;
			key->state = KEY_STATE_00;
		}

		break;

	default:
		/* nie powinno się zdarzyć - reset maszyny stanów */
		key->counter = 0U;
		key->event = KEY_NOT_PRESSED_EVENT;
		key->state = KEY_STATE_00;
		break;
	}
}

