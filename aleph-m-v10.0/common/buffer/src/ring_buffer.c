#include "../ring_buffer.h"

#include <stdbool.h>
#include <stdint.h>

void ring_buffer_init(ring_buffer_st *const buffer, uint8_t *const data,
                      uint8_t size)
{
	buffer->head = 0U;
	buffer->tail = 0U;
	buffer->count = 0U;
	buffer->data = data;
	buffer->size = size;
}

void ring_buffer_reset(ring_buffer_st *const buffer)
{
	buffer->head = 0U;
	buffer->tail = 0U;
	buffer->count = 0U;
}

uint8_t ring_buffer_capacity(ring_buffer_st const *const buffer)
{
	return (buffer->size);
}

uint8_t ring_buffer_count(ring_buffer_st const *const buffer)
{
	return (buffer->count);
}

uint8_t ring_buffer_space(ring_buffer_st const *const buffer)
{
	return ((uint8_t) (buffer->size - buffer->count));
}

bool ring_buffer_is_empty(ring_buffer_st const *const buffer)
{
	uint8_t tmp = false;

	if (0U == buffer->count) {
		tmp = true;
	}

	return (tmp);
}

bool ring_buffer_is_full(ring_buffer_st const *const buffer)
{
	uint8_t tmp = false;

	if (buffer->size == buffer->count) {
		tmp = true;
	}

	return (tmp);
}

ring_buffer_status_et ring_buffer_get(ring_buffer_st *const buffer,
                                      uint8_t *data)
{
	ring_buffer_status_et status = RING_BUFFER_FAIL;

	if (0U < buffer->count) {
		*data = buffer->data[buffer->tail];
		buffer->tail = (uint8_t) ((buffer->tail + 1U)
		                & (buffer->size - 1U));
		buffer->count = (uint8_t) (buffer->count - 1U);
		status = RING_BUFFER_SUCCESS;
	}

	return (status);
}

void ring_buffer_overwrite_put(ring_buffer_st *const buffer, uint8_t data)
{
	if (buffer->size == buffer->count) {
		buffer->tail = (uint8_t) ((buffer->tail + 1U)
		                & (buffer->size - 1U));
		buffer->count = (uint8_t) (buffer->count - 1U);
	}

	buffer->data[buffer->head] = data;
	buffer->head = (uint8_t) ((buffer->head + 1U) & (buffer->size - 1U));
	buffer->count = (uint8_t) (buffer->count + 1U);
}

ring_buffer_status_et ring_buffer_reject_put(ring_buffer_st *const buffer,
                                             uint8_t data)
{
	ring_buffer_status_et status = RING_BUFFER_FAIL;

	if (buffer->size > buffer->count) {
		buffer->data[buffer->head] = data;
		buffer->head = (uint8_t) ((buffer->head + 1U)
		                & (buffer->size - 1U));
		buffer->count = (uint8_t) (buffer->count + 1U);
		status = RING_BUFFER_SUCCESS;
	}

	return (status);
}

