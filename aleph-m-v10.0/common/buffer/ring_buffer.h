/*******************************************************************************
 *
 * bufor pierścieniowy
 *
 * rozmiar bufora musi być potęgą 2 (szybka arytmetyka indeksów)
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
	volatile uint8_t head;
	volatile uint8_t tail;
	volatile uint8_t count;
	uint8_t *data;
	uint8_t size;
} ring_buffer_st;

typedef enum {
	RING_BUFFER_SUCCESS,
	RING_BUFFER_FAIL
} ring_buffer_status_et;

/*
 * inicjuje bufor
 *
 * @buffer	wskaźnik do bufora
 * @data	wskaźnik do kontenera przechowującego dane
 * @size	rozmiar kontenera przechowującego dane, musi być potęgą 2
 */
void ring_buffer_init(ring_buffer_st *const buffer, uint8_t *const data,
                      uint8_t size) __attribute__((nonnull));

/*
 * reset, wyczyszczenie bufora
 *
 * @buffer	wskaźnik do bufora kołowego
 */
void ring_buffer_reset(ring_buffer_st *const buffer) __attribute__((nonnull));

/*
 * całkowita pojemność bufora
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		całkowita pojemność bufora
 */
uint8_t ring_buffer_capacity(ring_buffer_st const *const buffer) __attribute__((nonnull,pure));

/*
 * ilość elementów znajdujących się w buforze
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		ilość elementów w buforze
 */
uint8_t ring_buffer_count(ring_buffer_st const *const buffer) __attribute__((nonnull));

/*
 * ilość wolnego miejsca w buforze
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		ilość wolnego miejsca w buforze
 */
uint8_t ring_buffer_space(ring_buffer_st const *const buffer) __attribute__((nonnull));

/*
 * testuje czy bufor jest pusty
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		true - bufor pusty
 * 		false - bufor przechowuje dane
 */
bool ring_buffer_is_empty(ring_buffer_st const *const buffer) __attribute__((nonnull));

/*
 * testuje czy bufor jest pełny
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		true - bufor pełny
 * 		false - jest dostępne miejsce w buforze
 */
bool ring_buffer_is_full(ring_buffer_st const *const buffer) __attribute__((nonnull));

/*
 * odczytuje element z bufora jeśli bufor nie jest pusty
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		RING_BUFFER_SUCCESS - dana została odczytana
 * 		RING_BUFFER_FAIL - bufor jest pusty
 */
ring_buffer_status_et ring_buffer_get(ring_buffer_st *const buffer,
                                      uint8_t *data) __attribute__((nonnull));

/*
 * zapisuje element do bufora, jeśli bufor jest pełny nadpisuje dane
 *
 * @buffer	wskaźnik do bufora kołowego
 */
void ring_buffer_overwrite_put(ring_buffer_st *const buffer, uint8_t data) __attribute__((nonnull));

/*
 * zapisuje element do bufora, jeśli bufor jest pełny odrzuca dane
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		RING_BUFFER_SUCCESS - dana została zapisana
 * 		RING_BUFFER_FAIL - bufor jest pełny, dana odrzucona
 */
ring_buffer_status_et ring_buffer_reject_put(ring_buffer_st *const buffer,
                                             uint8_t data) __attribute__((nonnull));

