/*******************************************************************************
 *
 * manipulowanie pinami io
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>

typedef struct {
	volatile uint8_t *port;
	uint8_t pin;
} io_st;

static inline __attribute__((always_inline, nonnull)) volatile uint8_t* ddrx_from_portx(
                volatile uint8_t *const portx)
{
	return (portx - 1UL);
}

static inline __attribute__((always_inline, nonnull)) volatile uint8_t* pinx_from_portx(
                volatile uint8_t *const portx)
{
	return (portx - 2UL);
}

static inline __attribute__((always_inline, nonnull)) void io_set_as_output(
                io_st const *const io)
{
	*(ddrx_from_portx(io->port)) |= (uint8_t) (1UL << (io->pin));
}

static inline __attribute__((always_inline, nonnull)) void io_set_as_input(
                io_st const *const io)
{
	*(ddrx_from_portx(io->port)) &= (uint8_t) (~(1UL << (io->pin)));
}

static inline __attribute__((always_inline, nonnull)) void io_set_high(
                io_st const *const io)
{
	*(io->port) |= (uint8_t) (1UL << (io->pin));
}

static inline __attribute__((always_inline, nonnull)) void io_set_low(
                io_st const *const io)
{
	*(io->port) &= (uint8_t) (~(1UL << (io->pin)));
}

static inline __attribute__((always_inline, nonnull)) void io_set_opposite(
                io_st const *const io)
{
	*(io->port) ^= (uint8_t) (1UL << (io->pin));
}

static inline __attribute__((always_inline, nonnull)) uint8_t io_read_pin(
                io_st const *const io)
{
	return ((uint8_t) ((*(pinx_from_portx(io->port))) >> (io->pin)) & 1UL);
}

